# Contributing

Let me know if you have ideas for improvement, or just been tinkering with this.
Open issues, create merge requests, fork it.

Patches welcome!
